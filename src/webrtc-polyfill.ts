// Because JsSIP relies on browser APIs we polyfill some of them.
import WebSocket from 'ws';

globalThis.window = globalThis.window ?? {};

// RTCPeerConnection is only really needed for audio but its presence will be checked by JSSIP
globalThis.window.RTCPeerConnection = {};
globalThis.WebSocket = WebSocket;
