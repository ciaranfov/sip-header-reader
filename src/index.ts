require('dotenv').config();

import JsSIP from 'jssip';
import _debug from 'debug';
import './webrtc-polyfill';

import { IncomingRTCSessionEvent } from 'jssip/lib/UA';

const debug = _debug('sip_client');

async function main() {
  const socket = new JsSIP.WebSocketInterface(process.env.SIP_WSS_URI);
  const ua = new JsSIP.UA({
    sockets: [socket],
    uri: process.env.SIP_CLIENT_URI,
    password: process.env.SIP_CLIENT_PASS,
    register: true,
  });

  ua.start();

  ua.on('connected', () => {
    ua.register();
  });

  ua.on('disconnected', () => {
    ua.unregister();
  });

  ua.on('registered', () => {
    debug('SIP registered');
  });

  ua.on('newRTCSession', async (event: IncomingRTCSessionEvent) => {
    debug(`NEW REQUEST`);
    const request = event.request;
    
    debug(`Call-Id: ${request.getHeader('Call-ID')}`); // Available in parloa
    debug(`Caller number: ${request.from.display_name}`); // Available in parloa
    
    // debug(`Headers:`);
    // const headers = Reflect.get(request, 'headers') ?? {};
    // for (const key of Object.keys(headers)) {
    //   for (const value of headers[key]) {
    //     debug(`${key}: ${value.raw}`)
    //   }
    // }
    
    for (const u2u of request.getHeaders('User-To-User') ?? []) {
      // Extract info from User-To-User header
      debug(u2u);
    }
    
    // Store Call-Id, Caller number and User-To-User header info
    
    const session = event.session;
    session.terminate({ status_code: 486 }); // 486 Busy Here (hangup)
  });
}

if (require.main === module) {
  main();
}
