# SIP Header Reader

The scope of this application is to register itself as a sip client in order to receive SIP INVITE requests but NEVER to accept any call. This allows the application to read the headers sent within the SIP INVITE
request; the call will be accepted by another party.
